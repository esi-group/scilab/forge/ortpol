// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Check that int Hn(x)^2 w(x) dx=sqrt(2pi) n!
// for w(x)=exp(-x^2/2)
function y=sqHew(x, n)
    w=hermite_weight(x)
    P=hermite_eval(x,n)
    y=P^2*w
endfunction

for n = 1:5
    [v,err]=intg(-7,7,list(sqHew,n));
    I=hermite_norm(n);
    assert_checkalmostequal(I,sqrt(v),1.e-4);
    if (%f) then
        d=assert_computedigits(I,sqrt(v));
        mprintf("n=%d, I=%f, v=%f, digits=%f\n",n,I,sqrt(v),d)
    end
end
