// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/Hermite_polynomials

x=poly(0,"x");
//
y=chebyshev_poly(0);
L=1+x-x; // Trick to get a polynomial equal to 1
assert_checkequal(L,y);
//
y=chebyshev_poly(1);
L=x;
assert_checkequal(L,y);
//
y=chebyshev_poly(2);
L=2*x^2-1;
assert_checkequal(L,y);
//
y=chebyshev_poly(3);
L=4*x^3-3*x;
assert_checkequal(L,y);
//
// Check values
x=linspace(-1,1,100);
for n=0:15
    y1=horner(chebyshev_poly(n),x);
    y2=chebyshev_eval(x,n);
    assert_checkalmostequal(y1,y2);
end
