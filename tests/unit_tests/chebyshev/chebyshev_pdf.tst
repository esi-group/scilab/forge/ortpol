// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=linspace(-0.99,0.99,10);
y=chebyshev_pdf(x);
exact=[
    2.25643895684031648  
    0.49888400053472370  
    0.38113428830765378  
    0.33719948718216808  
    0.32025331550832709  
    0.32025331550832709  
    0.33719948718216808  
    0.38113428830765378  
    0.49888400053472370  
    2.25643895684031648  
];
assert_checkalmostequal(y',exact);
//
// Check integral
v=intg(-1,1,chebyshev_pdf);
assert_checkalmostequal(v,1);
//
// Check consistency with respect to weight
a=-1;
b=1;
I=intg(a,b,chebyshev_weight);
x=linspace(-0.99,0.99,10);
y1=chebyshev_pdf(x);
y2=chebyshev_weight(x);
assert_checkalmostequal(y1,y2/I);
