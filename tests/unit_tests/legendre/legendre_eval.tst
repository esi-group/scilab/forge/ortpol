// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/Hermite_polynomials

x=linspace(-1,1,100);
//
n=0;
y=legendre_eval(x,n);
assert_checkalmostequal(y,ones(1,100));
//
n=1;
y=legendre_eval(x,n);
assert_checkalmostequal(y,x);
//
n=2;
y=legendre_eval(x,n);
assert_checkalmostequal(y,(3*x.^2-1)/2);
//
n=3;
y=legendre_eval(x,n);
assert_checkalmostequal(y,(5*x.^3-3*x)/2);
//
n=4;
y=legendre_eval(x,n);
assert_checkalmostequal(y,(35*x.^4-30*x.^2+3)/8);

//
x=linspace(-1,1,100);
y0=legendre_eval(x,0);
y1=legendre_eval(x,1);
y2=legendre_eval(x,2);
y3=legendre_eval(x,3);
h=scf();
plot(x,y0,"r-")
plot(x,y1,"g-")
plot(x,y2,"b-")
plot(x,y3,"c:")
xtitle("Legendre Polynomials","x","He(x)")
legend(["n=0","n=1","n=2","n=3"]);
h.children.data_bounds(1:2,1)=[-1;1];
h.children.data_bounds(1:2,2)=[-1.1;1.1];
h.children.children(4).children.thickness=3;
h.children.children(5).children.thickness=2;
h.children.children(5).children.line_style=5;

