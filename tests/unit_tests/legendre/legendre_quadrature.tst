// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://keisan.casio.com/exec/system/1280624821
[x,w]=legendre_quadrature(6);
nodes=[
-0.932469514203152028
-0.661209386466264514
-0.238619186083196909
 0.238619186083196909
 0.661209386466264514
 0.932469514203152028
];
weights=[
0.171324492379170345
0.36076157304813861
0.46791393457269105
0.46791393457269105
0.36076157304813861
0.171324492379170345
];
assert_checkalmostequal(nodes,x,1.e-14);
assert_checkalmostequal(weights,w,1.e-14);

// Reference
// Numerical Mathematics, 
// Alfio Quarteroni, Riccardo Sacco, Fausto Saleri
// Example 10.1, p.437
// alpha=0 : f is C1
// alpha=1 : f is C2
// alpha=2 : f is C3

function y=myfunction(x,alpha)
    y=abs(x).^(alpha+3/5)
endfunction
n=30;
alpha=2;
// With Scilab
expected=intg(-1,1,list(myfunction,alpha));
[x,w]=legendre_quadrature(n);
y=myfunction(x,alpha);
weight=legendre_weight(x);
computed=sum(y.*w./weight);
assert_checkalmostequal(expected,computed,1.e-2);
