// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

I=legendre_norm(0);
assert_checkequal(I,sqrt(2));
//
I=legendre_norm(1);
assert_checkequal(I,sqrt(2/3));
//
I=legendre_norm(2);
assert_checkequal(I,sqrt(0.4));
//
I=legendre_norm(3);
assert_checkequal(I,sqrt(2/7));

// Check int Ln(x)^2 w(x) dx
function y=sqLw(x, n)
    w=legendre_weight(x)
    Px=legendre_eval(x,n)
    y=Px^2*w
endfunction

for n = 1:5
    [v,err]=intg(-1,1,list(sqLw,n),1.e-4,1.e-4);
    I=legendre_norm(n);
    assert_checkalmostequal(sqrt(v),I,[],1.e-3);
end
