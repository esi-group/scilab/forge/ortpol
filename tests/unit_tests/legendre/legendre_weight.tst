// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=linspace(-1,1,10);
y=legendre_weight(x);
exact=ones(x);
assert_checkalmostequal(y,exact);
