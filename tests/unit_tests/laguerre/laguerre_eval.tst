// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/Hermite_polynomials

x=linspace(0,10,100);
//
n=0;
y=laguerre_eval(x,n);
assert_checkalmostequal(y,ones(1,100));
//
n=1;
y=laguerre_eval(x,n);
assert_checkalmostequal(y,1-x);
//
n=2;
y=laguerre_eval(x,n);
assert_checkalmostequal(y,(x.^2-4*x+2)/2);
//
n=3;
y=laguerre_eval(x,n);
assert_checkalmostequal(y,(-x.^3+9*x.^2-18*x+6)/6);
//
n=4;
y=laguerre_eval(x,n);
assert_checkalmostequal(y,(x.^4-16*x.^3+72*x.^2-96*x+24)/24);

//
x=linspace(0,10,100);
y0=laguerre_eval(x,0);
y1=laguerre_eval(x,1);
y2=laguerre_eval(x,2);
y3=laguerre_eval(x,3);
h=scf();
plot(x,y0,"r-")
plot(x,y1,"g-")
plot(x,y2,"b-")
plot(x,y3,"c:")
xtitle("Laguerre Polynomials","x","He(x)")
legend(["n=0","n=1","n=2","n=3"]);
h.children.data_bounds(1:2,1)=[0;7];
h.children.data_bounds(1:2,2)=[-5;5];
h.children.children(4).children.thickness=3;
h.children.children(5).children.thickness=2;
h.children.children(5).children.line_style=5;

