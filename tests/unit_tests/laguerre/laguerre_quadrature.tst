// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://keisan.casio.com/exec/system/1281279441
[x,w]=laguerre_quadrature(6);
nodes=[
0.222846604179260689
1.18893210167262303
2.99273632605931408
5.7751435691045105
9.83746741838258992
15.9828739806017018
];
weights=[
0.45896467394996359
0.417000830772121
0.11337338207404498
0.01039919745314907
2.610172028149321E-4
8.985479064296212E-7
];
assert_checkalmostequal(nodes,x,1.e-14);
assert_checkalmostequal(weights,w,1.e-11);

// http://www.wolframalpha.com/

function y=myfunction(x)
    y=sqrt(x).*exp(-x)
endfunction
n=10;
exact=sqrt(%pi)/2;
[x,w]=laguerre_quadrature(n);
y=myfunction(x);
weight=laguerre_weight(x);
computed=sum(y.*w./weight);
assert_checkalmostequal(exact,computed,1.e-2);
