// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

for n = 1:5
    I=laguerre_norm(n);
    assert_checkequal(I,1);
end

// Check that int Ln(x)^2 w(x) dx=1
// for w(x)=exp(-x)
function y=sqLw(x, n)
    w=laguerre_weight(x)
    Px=laguerre_eval(x,n)
    y=Px^2*w
endfunction

for n = 1:5
    [v,err]=intg(0,30,list(sqLw,n),1.e-4,1.e-4);
    I=laguerre_norm(n);
    assert_checkalmostequal(sqrt(v),I,[],1.e-3);
end
