// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

x=poly(0,"x");
//
y=laguerre_poly(0);
L=1+x-x; // Trick to get a polynomial equal to 1
assert_checkequal(L,y);
//
y=laguerre_poly(1);
L=1-x;
assert_checkequal(L,y);
//
y=laguerre_poly(2);
L=2-4*x+x^2;
assert_checkequal(L,2*y);
//
y=laguerre_poly(3);
L=6-18*x+9*x^2-x^3;
assert_checkequal(L,6*y);
//
// Check values
x=linspace(0,10,100);
for n=0:15
    y1=horner(laguerre_poly(n),x);
    y2=laguerre_eval(x,n);
    assert_checkalmostequal(y1,y2);
end
