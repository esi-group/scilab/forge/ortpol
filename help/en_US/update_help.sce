// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.
//
cwd = get_absolute_file_path("update_help.sce");
modulename = "ortpol";
//
// Generate the Laguerre help pages
helpdir = fullfile(cwd,"laguerre");
funmat = [
  "laguerre_weight"
  "laguerre_pdf"
  "laguerre_poly"
  "laguerre_eval"
  "laguerre_norm"
  "laguerre_variance"
  "laguerre_quadrature"
  ];
macrosdir = fullfile(cwd,"..","..","macros","laguerre");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the Hermite help pages
helpdir = fullfile(cwd,"hermite");
funmat = [
  "hermite_weight"
  "hermite_pdf"
  "hermite_poly"
  "hermite_eval"
  "hermite_norm"
  "hermite_variance"
  "hermite_quadrature"
  ];
macrosdir = fullfile(cwd,"..","..","macros","hermite");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the Chevyshev help pages
helpdir = fullfile(cwd,"chebyshev");
funmat = [
  "chebyshev_weight"
  "chebyshev_pdf"
  "chebyshev_poly"
  "chebyshev_eval"
  "chebyshev_norm"
  "chebyshev_variance"
  "chebyshev_quadrature"
  ];
macrosdir = fullfile(cwd,"..","..","macros","chebyshev");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the Legendre help pages
helpdir = fullfile(cwd,"legendre");
funmat = [
  "legendre_weight"
  "legendre_pdf"
  "legendre_poly"
  "legendre_eval"
  "legendre_norm"
  "legendre_variance"
  "legendre_quadrature"
  ];
macrosdir = fullfile(cwd,"..","..","macros","legendre");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the main help pages
helpdir = fullfile(cwd);
funmat = [
  "ortpol_quadrature"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
