// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// Numerical Mathematics, 
// Alfio Quarteroni, Riccardo Sacco, Fausto Saleri
// Example 10.1, p.437

function dem_quadconvergence()
    function y=myhumps(x)
        y=1 ./ ((x-0.3).^2+0.01) + 1 ./ ((x-0.9).^2+0.04)
    endfunction
    xmin=0
    xmax=1
    // Graphics
    scf();
    x=linspace(xmin,xmax,500);
    y=myhumps(x);
    plot(x,y,"r-")
    xlabel("X")
    ylabel("Y")
    title("Humps")
    // Exact (Wolfram Alpha)
    Iexact=35.85832539549867508950089
    mprintf("(Exact) I=%f\n",I)
    // Chebyshev quadrature
    nmax=500
    abserr=zeros(nmax,1)
    for n=1:nmax
        // 1. Compute nodes and weights
        [nodes,w]=chebyshev_quadrature(n);
        // 2. Compute polynomial's weight function
        weight=chebyshev_weight(nodes);
        // 3. Scale the notes into [xmin,xmax]
        t=(nodes+1)/2 // in [0,1]
        x=xmin+(xmax-xmin)*t // in [xmin,xmax]
        // 4. Scale the weights
        w=w*(xmax-xmin)/2
        // 4. Evaluate the function
        y=myhumps(x);
        // 5. Approximate the integral
        I=sum(w.*y./weight);
        abserr(n)=abs(Iexact-I);
    end
    scf();
    narray=(1:nmax)'
    plot(narray,abserr,"bo");
    plot(narray,1 ./(narray.^2),"r-");
    xlabel("Number of nodes")
    ylabel("Absolute Error")
    legend(["Chebyshev","$1/n^2$"])
    a=gca()
    a.log_flags="lln"
endfunction 
dem_quadconvergence();
clear dem_quadconvergence
