// Copyright (C) 2013 - 2015 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function demo_laguerre()
    mprintf("See the common digits in the evaluation of Ln(x), \n")
    mprintf("evaluated from the polynomial, \n")
    mprintf("or from the recurrence.\n")
    mprintf("The recurrence is, in fact, much more accurate.\n")

    R=[];
    nmax=100;
    xmax=21;
    i=0;
    j=0;
    for x=1:4:xmax
        i=i+1;
        j=0;
        for n=1:10:nmax
            j=j+1;
            L=laguerre_poly(n);
            y1=horner(L,x);
            y2=laguerre_eval(x,n);
            d=assert_computedigits(y1,y2,2);
            d=floor(d);
            R(i,j)=d;
        end
    end
    // Create a bubble chart for the data
    h=scf();
    xtitle("Area=Number of digits","x","n");
    bubblematrix(1:4:xmax,1:10:nmax,R,-2,1.);
endfunction
demo_laguerre();
clear demo_laguerre
