// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// This script shows how the Gram-Schmidt orthogonalisation 
// algorithm can be used in order to produce a 
// set of polynomials orthonormal with respect to an arbitrary 
// weight function. 

// In this example, we choose the Legendre weight 
// on the interval [-1,1]. 
// The generated polynomials are multiples of the 
// classical unnormalized Legendre polynomials. 

// Reference
// Jim Lambers
// MAT 415/515
// Fall Semester 2013-14
// Lecture 3 Notes
// http://www.math.usm.edu/lambers/mat415/lecture3.pdf

function dem_gramschmidt()
    function y=myintegrand(x,__f__,__g__,__w__)
        if ( typeof(__f__) == "list" ) then  
            // List or tlist
            __f__f = __f__(1); 
            yf = __f__f ( x , __f__(2:$)); 
        elseif (typeof(__f__) == "polynomial")
            yf = horner(__f__,x)
        else
            // Macro or compiled macro
            yf = __f__ ( x );
        end
        if ( typeof(__g__) == "list" ) then  
            // List or tlist
            __g__f = __g__(1); 
            yg = __g__f ( x , __g__(2:$)); 
        elseif (typeof(__g__) == "polynomial")
            yg = horner(__g__,x)
        else
            // Macro or compiled macro
            yg = __g__ ( x );
        end
        if ( typeof(__w__) == "list" ) then  
            // List or tlist
            __w__f = __w__(1); 
            yw = __w__f ( x , __w__(2:$)); 
        else
            // Macro or compiled macro
            yw = __w__ ( x );
        end
        y=yf.*yg.*yw
        // plot(x,zeros(x),"bo")
    endfunction
    function I=myscalarproduct(xmin,xmax,f,g,weight,abserr)
        // Compute the scalar product of f and g in the sense 
        // of the weighted integral. 
        I=intg(xmin,xmax,list(myintegrand,f,g,weight),abserr)
    endfunction

    //
    xmin=-1.
    xmax=1.
    abserr=1.e-5
    // Compute P0
    P0=poly(1.,"x","coeff");
    a=myscalarproduct(xmin,xmax,P0,P0,legendre_weight,abserr)
    P0=P0/sqrt(a)
    // Compute P1
    P1=poly([0.,1.],"x","coeff");
    a=myscalarproduct(xmin,xmax,P1,P0,legendre_weight,abserr)
    P1=P1-P0*a
    a=myscalarproduct(xmin,xmax,P1,P1,legendre_weight,abserr)
    P1=P1/sqrt(a)
    // Compute P2
    P2=poly([0.,0.,1.],"x","coeff");
    a=myscalarproduct(xmin,xmax,P2,P0,legendre_weight,abserr)
    P2=P2-a*P0
    a=myscalarproduct(xmin,xmax,P2,P1,legendre_weight,abserr)
    P2=P2-a*P1
    a=myscalarproduct(xmin,xmax,P2,P2,legendre_weight,abserr)
    P2=P2/sqrt(a)
    //
    // Loop over n
    allpolys=list()
    nmax=5
    for i = 0:nmax
        // Create monom
        coeffmonoms=zeros(1,i+1)
        coeffmonoms($)=1.
        Pi=poly(coeffmonoms,"x","coeff");
        // Orthogonalize
        for j=0:i-1
            Pj=allpolys(j+1)
            a=myscalarproduct(xmin,xmax,Pi,Pj,legendre_weight,abserr)
            Pi=Pi-a*Pj
        end
        // Normalize
        a=myscalarproduct(xmin,xmax,Pi,Pi,legendre_weight,abserr)
        Pi=Pi/sqrt(a)
        // Append to the list
        allpolys(i+1)=Pi
    end
    mprintf("Computed with Gram-Schmidt:\n")
    disp(allpolys)
    //
    // Compare with Classical Legendre polynomials, normalized
    mprintf("From legendre_poly and legendre_norm:\n")
    for i=0:nmax
        P=legendre_poly(i);
        P=P/legendre_norm(i);
        disp(P)
    end
endfunction 
dem_gramschmidt();
clear dem_gramschmidt
