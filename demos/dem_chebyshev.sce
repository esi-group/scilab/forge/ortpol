// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function demo_chebyshev()
    mprintf("See degree 10 Chebyshev polynomial.\n")

    x=linspace(-1,1,1000);
    y=chebyshev_eval(x,10);
    h=scf();
    plot(x,y,"r-")
    xtitle("Chebyshev Polynomial T10","x","T10(x)")

    h=gca()
    h.data_bounds(:,2)=[-1.1;1.1]

    T=chebyshev_poly(10);
    x=roots(T)
    plot(x,zeros(x),"bo")
endfunction
demo_chebyshev();
clear demo_chebyshev
