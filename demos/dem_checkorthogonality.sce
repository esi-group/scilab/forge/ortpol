// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// Numerical Mathematics, 
// Alfio Quarteroni, Riccardo Sacco, Fausto Saleri
// Example 10.1, p.437

function dem_checkorthogonality()
    function y=myintegrand(x,__f__,__g__,__w__)
        if ( typeof(__f__) == "list" ) then  
            // List or tlist
            __f__f = __f__(1); 
            yf = __f__f ( x , __f__(2:$)); 
        else
            // Macro or compiled macro
            yf = __f__ ( x );
        end
        if ( typeof(__g__) == "list" ) then  
            // List or tlist
            __g__f = __g__(1); 
            yg = __g__f ( x , __g__(2:$)); 
        else
            // Macro or compiled macro
            yg = __g__ ( x );
        end
        if ( typeof(__w__) == "list" ) then  
            // List or tlist
            __w__f = __w__(1); 
            yw = __w__f ( x , __w__(2:$)); 
        else
            // Macro or compiled macro
            yw = __w__ ( x );
        end
        y=yf.*yg.*yw
        // plot(x,zeros(x),"bo")
    endfunction
    //
    xmin=-1.0
    xmax=1.0
    P0=list(chebyshev_eval,0)
    P1=list(chebyshev_eval,1)
    //
    abserr=1.e-5
    I=intg(xmin,xmax,list(myintegrand,P0,P1,chebyshev_weight),abserr)
    mprintf("(P%d,P%d)=%f\n",0,1,I)
    // 
    // Check orthogonality : compute Gram matrix
    // If the polynomials are orthogonal, the Gram matrix 
    // is diagonal.
    // This is a difficult integration problem, because the 
    // integrand is Pi(x)*Pj(x)*w(x). 
    // This is a degree i+j polynomial, which is highly oscillatory. 
    function G=grammatrix(nmax,poly_eval,poly_weight,xmin,xmax,abserr,threshold)
        function I=myscalarproduct(xmin,xmax,f,g,weight,abserr)
            // Compute the scalar product of f and g in the sense 
            // of the weighted integral. 
            I=intg(xmin,xmax,list(myintegrand,f,g,weight),abserr)
        endfunction
        G=zeros(nmax,nmax)
        for i = 1:nmax
            for j=i:nmax
                Pi=list(poly_eval,i);
                Pj=list(poly_eval,j);
                I=myscalarproduct(xmin,xmax,Pi,Pj,poly_weight,abserr);
                if (abs(I)<threshold) then
                    I=0.
                end
                G(i,j)=I;
                if (j>i) then
                    G(j,i)=G(i,j);
                end
            end
        end
    endfunction
    // Legendre
    nmax=5
    abserr=1.e-5
    xmin=-1
    xmax=1
    threshold=1.e-10
    G=grammatrix(nmax,legendre_eval,legendre_weight,xmin,xmax,abserr);
    mprintf("Legendre Gramian Matrix\n")
    disp(G)
    // Chebyshev
    nmax=5
    abserr=1.e-6
    xmin=-1
    xmax=1
    threshold=1.e-10
    G=grammatrix(nmax,chebyshev_eval,chebyshev_weight,xmin,xmax,abserr);
    mprintf("Chebyshev Gramian Matrix\n")
    disp(G)
    // Hermite
    nmax=5
    abserr=1.e-3
    xmin=-10
    xmax=10
    threshold=1.e-10
    G=grammatrix(nmax,hermite_eval,hermite_weight,xmin,xmax,abserr);
    mprintf("Hermite Gramian Matrix\n")
    disp(G)
    // Laguerre
    nmax=5
    abserr=1.e-3
    xmin=0
    xmax=50
    threshold=1.e-10
    G=grammatrix(nmax,laguerre_eval,laguerre_weight,xmin,xmax,abserr);
    mprintf("Laguerre Gramian Matrix\n")
    disp(G)
endfunction 
dem_checkorthogonality();
clear dem_checkorthogonality
