// Copyright (C) 2015 - 2017 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// Numerical Mathematics, 
// Alfio Quarteroni, Riccardo Sacco, Fausto Saleri
// Example 10.1, p.437

function dem_quadrature()
    function y=myfunction(x,alpha)
        y=abs(x).^(alpha+3/5)
    endfunction
    alpha=0
    // With Scilab
    I=intg(-1,1,list(myfunction,alpha))
    mprintf("(intg) I=%f\n",I)
    // Chebyshev
    n=50
    mprintf("Number of nodes=%d\n",n)
    [x,w]=chebyshev_quadrature(n);
    y=myfunction(x,alpha);
    weight=chebyshev_weight(x);
    I=sum(y.*w./weight)
    mprintf("(Chebyshev) I=%f\n",I)
    // Legendre
    [x,w]=legendre_quadrature(n);
    y=myfunction(x,alpha);
    weight=legendre_weight(x);
    I=sum(y.*w./weight)
    mprintf("(Legendre) I=%f\n",I)
    // Hermite : impossible, car la fonction n'est pas bornée.
endfunction 
dem_quadrature();
clear dem_quadrature
