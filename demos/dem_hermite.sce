// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function demo_hermite()
    mprintf("\n")
    mprintf("1. Check that x**2 has a finite L2w(R) norm\n")
    mprintf("with Hermite weight\n")
    function y=Hermite1W(x)
        // The function x^2 w(x).
        w=hermite_weight(x)
        y=x.^2 .*w
    endfunction

    x=linspace(-4,4,1000);
    y=Hermite1W(x);
    scf();
    plot(x,y)
    xtitle("","x","$x^2w(x)$")

    [v,err]=intg(-10,10,Hermite1W);
    mprintf("Computed integral:%f\n",v)
    mprintf("Exact integral:%f\n",sqrt(2*%pi))
    /////////////////////////
    mprintf("\n")
    mprintf("2. Check that x^n has a finite L2w(R) norm.\n")
    function y=HermiteNW(x,n)
        // The function x^n w(x)
        w=hermite_weight(x)
        y=x.^n .*w
    endfunction

    I0=intg(-10,10,list(HermiteNW,0))
    exact=sqrt(2*%pi)
    mprintf("n=0, integral=%f, exact=%f\n",I0,exact)
    //
    I2=intg(-10,10,list(HermiteNW,2))
    exact=1*sqrt(2*%pi)
    mprintf("n=2, integral=%f, exact=%f\n",I2,exact)
    //
    I4=intg(-10,10,list(HermiteNW,4))
    exact=1*3*sqrt(2*%pi)
    mprintf("n=4, integral=%f, exact=%f\n",I4,exact)
    //
    I6=intg(-10,10,list(HermiteNW,6))
    exact=1*3*5*sqrt(2*%pi)
    mprintf("n=6, integral=%f, exact=%f\n",I6,exact)
    //
    // For odd n:
    x=linspace(-4,4,1000);
    y=HermiteNW(x,3);
    scf();
    plot(x,y)
    xtitle("","x","$x^3w(x)$")
    /////////////////////////////////////
    mprintf("\n")
    mprintf("3. Check that He2 has a finite L2w(R) integral\n")
    function y=Hermite2W(x)
        // The function He2(x)^2 w(x)
        w=hermite_weight(x)
        P=hermite_eval(x,2)
        y=P.^2 .*w
    endfunction

    x=linspace(-4,4,1000);
    y=Hermite2W(x);
    scf();
    plot(x,y)
    xtitle("","x","$He_2^2w(x)$")

    [v,err]=intg(-10,10,Hermite2W)
    mprintf("Computed=%f\n",v)
    mprintf("Exact=%f\n",hermite_norm(2))
    /////////////////////////////////////
    //
    mprintf("\n")
    mprintf("4. Check that int Hn(x) w(x) dx=0 for n>=1\n")
    //
    function y=myf(x,n)
        Hx=hermite_eval(x,n)
        wx=hermite_pdf(x)
        y=Hx.*wx
    endfunction
    scf();
    k=1;
    for n=0:5
        x=linspace(-10,10,100);
        y=myf(x,n);
        subplot(2,3,k);
        plot(x,y,"r-");
        str=msprintf("n=%d",n);
        xtitle(str,"x","He(x)w(x)");
        k=k+1;
    end
    //
    atol=1.e-4;
    for n=1:10
        v=intg(-7,7,list(myf,n),atol);
        mprintf("n=%d, Integral=%f\n",n,v)
    end
endfunction
demo_hermite();
clear demo_hermite
