Orthogonal Polynomials Toolbox

Purpose
=======

This module allows to create and manage orthogonal polynomials. 
Emphasis is on accuracy of the functions. 

Features
--------

* ortpol_quadrature : Returns nodes and weights of a quadrature from alphas and betas

For each polynomial "x", the following functions are 
available :

 * x_eval � Evaluate polynomial
 * x_norm � Compute L2 norm
 * x_pdf � Evaluate probability distribution function
 * x_poly � Create polynomial
 * x_quadrature � Returns nodes and weights of Gauss quadrature
 * x_variance � Variance of polynomial
 * x_weight � Evaluate weight function

with :
 * x="chebyshev" : Chebyshev Polynomials
 * x="hermite" : Hermite Polynomials
 * x="laguerre" : Laguerre Polynomials
 * x="legendre" : Legendre Polynomials

Dependencies
------------

 * This module depends on the Scilab >=v5.5
 * This module depends on the apifun module (>=0.2).
 * This module depends on the Stixbox module (>=2.2).

Authors
=======

 * Copyright (C) 2013-2015 - Michael Baudin

Forge
-----

http://forge.scilab.org/index.php/p/ortpol/

ATOMS
-----

http://atoms.scilab.org/toolboxes/ortpol

TODO
====

TODO list
 * Chebyshev
