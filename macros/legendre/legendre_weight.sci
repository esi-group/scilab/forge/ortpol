// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=legendre_weight(x)
    // Legendre weight function
    //  
    // Calling Sequence
    // y=legendre_weight(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the weight
    //
    // Description
    // Computes the Legendre weight function:
    //
    // <latex>
    // w(x)=1
    // </latex>
    //
    // for x in [-1,1].
    //
    // Examples
    // x=linspace(-1,1,1000);
    // y=legendre_weight(x);
    // h=scf();
    // plot(x,y)
    // xtitle("Legendre weight","x","w(x)")
    // h.children.data_bounds(:,2)=[-0.1;1.1];
    //
    // v=intg(-1,1,legendre_weight)
    // 2
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    apifun_checkrange("legendre_weight",x,"x",1,-1,1)

    y=ones(x)
endfunction
