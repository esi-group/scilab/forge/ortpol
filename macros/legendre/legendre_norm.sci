// Copyright (C) 2013-2017 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=legendre_norm(n)
    // Compute Legendre L2 norm
    //  
    // Calling Sequence
    // y=legendre_norm(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the norm
    //
    // Description
    // Returns :
    //
    // <latex>
    // \left( \int_{-1}^1 P_n(x)^2 w(x) dx \right^{1/2}
    // </latex>
    //
    // where Pn is Legendre polynomial.
    //
    // Examples
    // // Check that int Pn(x)^2 w(x) dx=2/(2n+1).
    //    function y=sqPw(x,n)
    //        y=legendre_eval(x,n)^2
    //    endfunction
    //    for n = 0:5
    //        [v,err]=intg(-1,1,list(sqPw,n));
    //        I=legendre_norm(n)**2;
    //        disp([n,I,v])
    //    end
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("legendre_norm",n,"n",1,0)
    apifun_checkflint("legendre_norm",n,"n",1)

    y=sqrt(2/(2*n+1))
endfunction
