// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=legendre_variance(n)
    // Variance of Legendre polynomials
    //  
    // Calling Sequence
    // y=legendre_variance(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the variance
    //
    // Description
    // Compute the variance 
    //
    // <latex>
    // V(P_n(X))=\frac{1}{2n+1}
    // </latex>
    //
    // for n=0,1,2,..., where X is a uniform random variable 
    // in the interval [-1,1].
    //
    // Examples
    // legendre_variance(0)
    // legendre_variance(1)
    // legendre_variance(2)
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("legendre_variance",n,"n",1,0)
    apifun_checkflint("legendre_variance",n,"n",1)

    if (n==0) then
        y=0
    else
        y=1 ./(2*n+1)
    end
endfunction
