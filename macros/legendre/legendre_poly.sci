// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=legendre_poly(n)
    // Create Legendre polynomial
    //  
    // Calling Sequence
    // y=legendre_poly(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a polynomial
    //
    // Description
    // Create a Legendre polynomial of degree n at point x. 
    // To evaluate it, we can use the horner function, 
    // but legendre_eval might be faster and more accurate.
    //
    // The Legendre polynomials are orthogonals with respect to 
    // the scalar product :
    //
    // <latex>
    // (f,g)=\int_{-1}^{1} f(x) g(x) w(x) dx
    // </latex>
    //
    // where w(x) is the Legendre weight.
    //
    // Examples
    // for n=0:10
    //     y=legendre_poly(n)
    // end
    // 
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Legendre_polynomials

    apifun_checkgreq("legendre_poly",n,"n",1,0)
    apifun_checkflint("legendre_poly",n,"n",1)

    if (n==0) then
        y=poly(1,"x","coeff")
    elseif (n==1) then
        y=poly([0 1],"x","coeff")
    else
        polyx=poly([0 1],"x","coeff")
        // y(n-2)
        yn2=poly(1,"x","coeff")
        // y(n-1)
        yn1=polyx
        for k=2:n
            y=((2*k-1)*polyx*yn1-(k-1)*yn2)/k
            yn2=yn1
            yn1=y
        end
    end
endfunction
