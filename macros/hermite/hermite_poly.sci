// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=hermite_poly(n)
    // Create Hermite polynomial
    //  
    // Calling Sequence
    // y=hermite_poly(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a polynomial
    //
    // Description
    // Create a Hermite polynomial of degree n at point x. 
    //
    // The Hermite polynomials are orthogonals with respect to 
    // the scalar product :
    //
    // <latex>
    // (f,g)=\int_{0}^{\infty} f(x) g(x) w(x) dx
    // </latex>
    //
    // where w(x) is the Hermite weight.
    //
    // To evaluate it, we can use the horner function, 
    // but hermite_eval might be faster and more accurate.
    //
    // Examples
    //    for n=0:10
    //        y=hermite_poly(n);
    //        disp(y)
    //    end
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Gaussian_integral
    // http://en.wikipedia.org/wiki/Hermite_polynomials
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    apifun_checkgreq("hermite_poly",n,"n",1,0)
    apifun_checkflint("hermite_poly",n,"n",1)

    if (n==0) then
        y=poly(1,"x","coeff")
    elseif (n==1) then
        y=poly([0 1],"x","coeff")
    else
        polyx=poly([0 1],"x","coeff")
        // y(n-2)
        yn2=poly(1,"x","coeff")
        // y(n-1)
        yn1=polyx
        for k=2:n
            y=polyx*yn1-(k-1)*yn2
            yn2=yn1
            yn1=y
        end
    end
endfunction
