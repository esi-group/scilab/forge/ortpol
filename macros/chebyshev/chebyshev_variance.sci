// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=chebyshev_variance(n)
    // Variance of Chebyshev polynomials
    //  
    // Calling Sequence
    // y=chebyshev_variance(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the variance
    //
    // Description
    // Compute the variance 
    //
    // <latex>
    // V(T_0(X))=0
    // </latex>
    //
    // and
    //
    // <latex>
    // V(T_n(X))=0.5
    // </latex>
    //
    // for n greater than 0, where X has the Chebyshev distribution.
    //
    // Examples
    // chebyshev_variance(0)
    // chebyshev_variance(1)
    // chebyshev_variance(2)
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    apifun_checkgreq("chebyshev_variance",n,"n",1,0)
    apifun_checkflint("chebyshev_variance",n,"n",1)

    if (n==0) then
        y=0
    else
        y=0.5
    end
endfunction
