// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y=chebyshev_pdf(x)
    // Chebyshev PDF
    //  
    // Calling Sequence
    // y=chebyshev_pdf(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the probability density
    //
    // Description
    // Computes the Chebyshev probability 
    // distribution function:
    //
    // <latex>
    // f(x)=\frac{1}{\pi \sqrt{1-x^2}}
    // </latex>
    //
    // for any real value x.
    // Its integral is equal to 1.
    //
    // Examples
    //    x=linspace(-0.99,0.99,1000);
    //    y=chebyshev_pdf(x);
    //    scf();
    //    plot(x,y)
    //    xtitle("Chebyshev PDF","x","f(x)")
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs ( "chebyshev_pdf" , rhs , 1 )
    apifun_checklhs ( "chebyshev_pdf" , lhs , 0:1 )
    //
    apifun_checkrange ( "chebyshev_pdf",x,"x",1,-1,1 )
    //
    y=1 ./sqrt(1-x.^2)/%pi
endfunction
