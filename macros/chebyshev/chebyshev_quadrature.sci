// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function [x,w]=chebyshev_quadrature(n)
    // Returns nodes and weights of Gauss-Chebyshev quadrature
    //  
    // Calling Sequence
    // [x,w]=chebyshev_quadrature(n)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, n>=0, the number of nodes.
    // x : a n-by-1 matrix of doubles, the nodes
    // w : a n-by-1 matrix of doubles, the weights
    //
    // Description
    // Returns nodes x and weights w of Gauss-Chebyshev quadrature.
    //
    // Examples
    // [x,w]=chebyshev_quadrature(6)
    //
    // Authors
    // Copyright (C) 2015 - Michael Baudin

    apifun_checkgreq("chebyshev_quadrature",n,"n",2,0)
    apifun_checkflint("chebyshev_quadrature",n,"n",2)

    //
    // Computes nodes
    k=(1:n)'
    x=-cos((2*k-1)*%pi/(2*n))
    //
    // Computes weights
    w=ones(n,1)*%pi/n
endfunction
