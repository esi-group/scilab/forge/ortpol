// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function y=laguerre_eval(x,n)
    // Evaluate Laguerre polynomial
    //  
    // Calling Sequence
    // y=laguerre_eval(x,n)
    //
    // Parameters
    // x : a matrix of doubles
    // n : a 1-by-1 matrix of doubles, integer value, n>=0.
    // y : a matrix of doubles, the function value
    //
    // Description
    // Evaluates a Laguerre polynomial of degree n at point x.
    //
    // The first Laguerre polynomials are
    //
    // <latex>
    // \begin{eqnarray}
    // L_0(x)&=&1, \\
    // L_1(x)&=&-x+1
    // \end{eqnarray}
    // </latex>
    //
    // The remaining Laguerre polynomials satisfy the recurrence:
    //
    // <latex>
    // \begin{eqnarray}
    // (n+1) L_{n+1}(x) = (2n+1-x) L_n(x) - n L_{n-1}(x),
    // \end{eqnarray}
    // </latex>
    //
    // for n=1,2,....
    //
    // Examples
    //    x=linspace(0,10,100);
    //    y0=laguerre_eval(x,0);
    //    y1=laguerre_eval(x,1);
    //    y2=laguerre_eval(x,2);
    //    y3=laguerre_eval(x,3);
    //    h=scf();
    //    plot(x,y0,"r-")
    //    plot(x,y1,"g-")
    //    plot(x,y2,"b-")
    //    plot(x,y3,"c:")
    //    xtitle("Laguerre Polynomials","x","He(x)")
    //    legend(["n=0","n=1","n=2","n=3"]);
    //    h.children.data_bounds(1:2,1)=[0;7];
    //    h.children.data_bounds(1:2,2)=[-5;5];
    //    h.children.children(4).children.thickness=3;
    //    h.children.children(5).children.thickness=2;
    //    h.children.children(5).children.line_style=5;
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("laguerre_eval",x,"x",1,0)
    apifun_checkgreq("laguerre_eval",n,"n",2,0)
    apifun_checkflint("laguerre_eval",n,"n",2)

    if (n==0) then
        y=ones(x)
    elseif (n==1) then
        y=1-x
    else
        // y(n-2)
        yn2=1
        // y(n-1)
        yn1=1-x
        for k=2:n
            y=((2*k-1-x).*yn1-(k-1)*yn2)/k
            yn2=yn1
            yn1=y
        end
    end
endfunction
