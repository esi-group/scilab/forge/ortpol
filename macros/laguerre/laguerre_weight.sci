// Copyright (C) 2013-2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function y = laguerre_weight ( x )
    // Laguerre weight function
    //  
    // Calling Sequence
    // y=laguerre_weight(x)
    //
    // Parameters
    // x : a matrix of doubles
    // y : a matrix of doubles, the weight
    //
    // Description
    // Computes the Exponential weight function:
    //
    // <latex>
    // w(x)=\exp(-x)
    // </latex>
    //
    // for x positive.
    //
    // Examples
    // x=linspace(0,4,1000);
    // y=laguerre_weight(x);
    // scf();
    // plot(x,y)
    // xtitle("Laguerre weight","x","w(x)")
    // 
    // // Check integral : must be 1.
    // v=intg(0,10,laguerre_weight)
    //
    // Authors
    // Copyright (C) 2013-2015 - Michael Baudin

    apifun_checkgreq("laguerre_weight",x,"x",1,0)
    y=exp(-x)
endfunction

