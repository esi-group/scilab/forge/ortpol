// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function ortpolBuilder()
    mode(-1);
    lines(0);
    setenv("__USE_DEPRECATED_STACK_FUNCTIONS__","YES");
    // Uncomment to make a Debug version
    // setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")
    try
        getversion("scilab");
    catch
        error(gettext("Scilab 5.0 or more is required."));  
    end
    // ====================================================================
    if ~with_module("development_tools") then
        error(msprintf(gettext("%s module not installed."),"development_tools"));
    end
    // ====================================================================
    TOOLBOX_NAME = "Ortpol";
    TOOLBOX_TITLE = "Ortpol";
    // ====================================================================
    toolbox_dir = get_absolute_file_path("builder.sce");
    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME , toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction 
ortpolBuilder();
clear ortpolBuilder
